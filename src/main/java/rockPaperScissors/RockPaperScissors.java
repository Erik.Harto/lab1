package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    

    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        
        while(true) {
            System.out.println("Let's play round "+ roundCounter);
            String userChoice = readInput("Your choice (Rock/Paper/Scissors)?").toLowerCase();

            if ((!userChoice.equals("rock")) && (!userChoice.equals("paper")) && (!userChoice.equals("scissors"))) {
                System.out.println("I do not understand " + userChoice + ". Could you try again?");
                continue;
            } else {
                
                Random rand = new Random();
                String computerChoice = rpsChoices.get(rand.nextInt(rpsChoices.size())).toLowerCase();
                String result = rpsSimulation(userChoice, computerChoice);
                System.out.println("Human chose " + userChoice + ", computer chose " + computerChoice + ". " + result);
                System.out.printf("Score: human %s, computer %s \n", humanScore, computerScore);

                String cont = readInput("Do you wish to continue playing? (y/n)?").toLowerCase();
                if (cont.equals("y")) {
                    roundCounter ++;
                } else if (cont.equals("n")) {
                    System.out.println("Bye bye :)");
                    break;
                } else {
                    System.out.println("The input was not accepted");
                    continue;
                }
            } 
        }
    }

    public String rpsSimulation(String userInput, String computerInput) {
        if (userInput.equals(computerInput)) {
            return " Its a tie!";
        } else if ((userInput.equals("rock") && computerInput.equals("scissors")) || 
        (userInput.equals("scissors") && computerInput.equals("paper")) ||
        (userInput.equals("paper") && computerInput.equals("rock"))) {
            humanScore++;
            return " Human wins!";
        } else if ((userInput.equals("scissors") && computerInput.equals("rock")) || 
        (userInput.equals("paper") && computerInput.equals("scissors")) ||
        (userInput.equals("rock") && computerInput.equals("paper"))) {
            computerScore++;
            return " Computer wins!"; 
        } else {
            return " invalid move";
        }
    }
    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
